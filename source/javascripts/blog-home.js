$(function() {
  $('.blog-card-excerpt').each(function() {
    var len = $(this).text().length;
    if (len > 140) {
      $(this).text($(this).text().substr(0, 140) + '...');
    }
  });
});


(function() {
  var videoButton = document.getElementById('video-button');
  var iframeContainer = document.getElementById('iframe-container');
  var iframeVideo = document.getElementById('iframe-video');
  var videoSource = 'https://www.youtube.com/embed/oK1TyjAitKU?autoplay=1';

  function showVideo() {
    iframeContainer.style.display = 'flex';
    iframeVideo.src = videoSource;
  }

  function hideVideo() {
    iframeContainer.style.display = 'none';
    iframeVideo.src = '';
  }

  function stopProp(event) {
    event.stopPropagation();
  }

  videoButton.addEventListener('click', showVideo);
  iframeContainer.addEventListener('click', hideVideo);
  iframeVideo.addEventListener('click', stopProp);
})();


(function() {
  var videoButton = document.getElementById('video-button-feat-1');
  var iframeContainer = document.getElementById('iframe-container');
  var iframeVideo = document.getElementById('iframe-video');
  var videoSource = 'https://www.youtube.com/embed/durK50IR9fk?autoplay=1';

  function showVideo() {
    iframeContainer.style.display = 'flex';
    iframeVideo.src = videoSource;
  }

  function hideVideo() {
    iframeContainer.style.display = 'none';
    iframeVideo.src = '';
  }

  function stopProp(event) {
    event.stopPropagation();
  }

  videoButton.addEventListener('click', showVideo);
  iframeContainer.addEventListener('click', hideVideo);
  iframeVideo.addEventListener('click', stopProp);
})();


(function() {
  var videoButton = document.getElementById('video-button-feat-2');
  var iframeContainer = document.getElementById('iframe-container');
  var iframeVideo = document.getElementById('iframe-video');
  var videoSource = 'https://www.youtube.com/embed/YUXeodJgke0?autoplay=1';

  function showVideo() {
    iframeContainer.style.display = 'flex';
    iframeVideo.src = videoSource;
  }

  function hideVideo() {
    iframeContainer.style.display = 'none';
    iframeVideo.src = '';
  }

  function stopProp(event) {
    event.stopPropagation();
  }

  videoButton.addEventListener('click', showVideo);
  iframeContainer.addEventListener('click', hideVideo);
  iframeVideo.addEventListener('click', stopProp);
})();


(function() {
  var videoButton = document.getElementById('video-button-feat-3');
  var iframeContainer = document.getElementById('iframe-container');
  var iframeVideo = document.getElementById('iframe-video');
  var videoSource = 'https://www.youtube.com/embed/hi2D0Se_VnA?autoplay=1';

  function showVideo() {
    iframeContainer.style.display = 'flex';
    iframeVideo.src = videoSource;
  }

  function hideVideo() {
    iframeContainer.style.display = 'none';
    iframeVideo.src = '';
  }

  function stopProp(event) {
    event.stopPropagation();
  }

  videoButton.addEventListener('click', showVideo);
  iframeContainer.addEventListener('click', hideVideo);
  iframeVideo.addEventListener('click', stopProp);
})();


(function() {
  var videoButton = document.getElementById('video-button-feat-4');
  var iframeContainer = document.getElementById('iframe-container');
  var iframeVideo = document.getElementById('iframe-video');
  var videoSource = 'https://www.youtube.com/embed/w6X4Ha1oC6I?autoplay=1';

  function showVideo() {
    iframeContainer.style.display = 'flex';
    iframeVideo.src = videoSource;
  }

  function hideVideo() {
    iframeContainer.style.display = 'none';
    iframeVideo.src = '';
  }

  function stopProp(event) {
    event.stopPropagation();
  }

  videoButton.addEventListener('click', showVideo);
  iframeContainer.addEventListener('click', hideVideo);
  iframeVideo.addEventListener('click', stopProp);
})();


